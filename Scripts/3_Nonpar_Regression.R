# Purpose: These functions make temporal trend analyis using non-parametric
# Mann-Kendall Theil-Sen trend analysis for intensity indices, and logistic
# regression for frequency indices.
#
# Programmer
# 	Joel Zeder: 11 December 2018, Zurich 

rm(list=ls())
setwd('~/Path/to/the/directory/where/you/keep/these/Scripts/')
source(file.path(getwd(),'/0_Basic_Settings.R'),echo=F)
source(file.path(getwd(),'/3_Nonpar_Regression_Fun.R'),echo=F)

## ----------------------------------------------------------------------------------------------
## SETTINGS AND FILE LOCATIONS
## ===========================

# Where to save plots to in this script:
path_plot <- '../Output/Plots/3_Nonpar_Trends/'

## ----------------------------------------------------------------------------------------------
## Read in datasets
## ================

# Load precip / temperature data:
load_csv_data(path_data)

# Get indices:# Get indices:
assign(x="ind",value=ind_search(df_Rx1d_d),envir=.GlobalEnv)

# Load results data:
assign(x="df_res_temp",value=load_create_res_temp(),envir=.GlobalEnv)

## ----------------------------------------------------------------------------------------------
## Visualise all (rescaled) Rx1d values within a heatmap
## =====================================================

series_tiles(df_Rx1d_d,"Rx1d")
series_tiles(df_Rx31d_d,"Rx31d")
series_tiles(df_R95p_d,"#R95p")

## ----------------------------------------------------------------------------------------------
## Fit non-parametric regression (time) from 1950-2016 -> See how trends evolve over time
## ======================================================================================

# Make plot of temporal evolution of trend-coef (sig/nonsig):
trend_evol(df_Rx1d_d,df_type="cont",index_name="Rx1d",
           origin_vec="all",sumfig=T,verbose=T)
trend_evol(df_Rx1d_d,df_type="cont",index_name="Rx1d",sumfig = T)
trend_evol(df_Rx3d_d,df_type="cont",index_name="Rx3d",sumfig = T)
trend_evol(df_Rx5d_d,df_type="cont",index_name="Rx5d",sumfig = T)
trend_evol(df_Rx7d_d,df_type="cont",index_name="Rx7d",sumfig = T)
trend_evol(df_Rx31d_d,df_type="cont",index_name="Rx31d",sumfig = T)
trend_evol(df_R95p_d,df_type="count",index_name="#R95p",sumfig = T,verbose=T)
trend_evol(df_R99p_d,df_type="count",index_name="#R99p",sumfig = T,verbose=T)

trend_evol(df_Rx1d_d,df_type="cont",index_name="Rx1d",origin_vec="ECA",sumfig = T)
trend_evol(df_R95p_d,df_type="count",index_name="#R95e",origin_vec="ECA",sumfig = T)

## ----------------------------------------------------------------------------------------------
## Fit non-parametric and logistic regression over total 
## =====================================================

source(paste(getwd(),'/3_Nonpar_Regression_Fun.R',sep=""),echo=F)
trend_map(df_mean_d,df_type="cont",index_name="Mean",plot_res=T,save_res=F,season_vec = "year")

trend_map(df_Rx1d_d,df_type="cont",index_name="Rx1d",plot_res=T,save_res=T,season_vec = "year")
trend_map(df_Rx3d_d,df_type="cont",index_name="Rx3d",plot_res=T,save_res=T)
trend_map(df_Rx5d_d,df_type="cont",index_name="Rx5d",plot_res=T,save_res=T)
trend_map(df_Rx7d_d,df_type="cont",index_name="Rx7d",plot_res=T,save_res=T)
trend_map(df_Rx31d_d,df_type="cont",index_name="Rx31d",plot_res=T,save_res=T)
trend_map(df_R95p_d,df_type="count",index_name="#R95p",plot_res=T,save_res=T)
trend_map(df_R99p_d,df_type="count",index_name="#R99p",plot_res=T,save_res=T)
join_dataframes(results=T)


## ----------------------------------------------------------------------------------------------
## Plot percentage of significant autocorrelated series
## ====================================================

# Plot barplots of percentages of stations showing significant autocorrelation:
for(bool_detrend in c(F,T)) {
  sign_acf(df_Rx1d_d,index_name="Rx1d",detrend=bool_detrend)
  sign_acf(df_Rx3d_d,index_name="Rx3d",detrend=bool_detrend)
  sign_acf(df_Rx5d_d,index_name="Rx5d",detrend=bool_detrend) # ,cs_Winter=T
  sign_acf(df_Rx7d_d,index_name="Rx7d",detrend=bool_detrend)
  sign_acf(df_Rx31d_d,index_name="Rx31d",detrend=bool_detrend)
  sign_acf(df_R95p_d,index_name="#R95p",detrend=bool_detrend)
  sign_acf(df_R99p_d,index_name="#R99p",detrend=bool_detrend)
}

# Plot map of stations with significant autocorrelation
sign_acf(df_R99p_d,index_name="#R99p",detrend=F,map_sign=T)
sign_acf(df_R95p_d,index_name="#R95p",detrend=F,map_sign=T)
sign_acf(df_Rx5d_d,index_name="Rx5d",detrend=F,map_sign=T)

## ----------------------------------------------------------------------------------------------
## Plot boxplots of RxXd und RXXp trends in regions
## ================================================

source(paste(getwd(),'/3_Nonpar_Regression_Fun.R',sep=""),echo=F)
index_vec <- c("Rx1d","Rx3d","Rx5d","Rx7d","Rx31d","#R95p","#R99p")

p1 <- plot_trend_RxXd_RXXp_region_season(index_vec[1],"region")
p2 <- plot_trend_RxXd_RXXp_region_season(index_vec[2],"region")
p3 <- plot_trend_RxXd_RXXp_region_season(index_vec[3],"region")
p4 <- plot_trend_RxXd_RXXp_region_season(index_vec[4],"region")
p5 <- plot_trend_RxXd_RXXp_region_season(index_vec[5],"region")
p6 <- plot_trend_RxXd_RXXp_region_season(index_vec[6],"region")
p7 <- plot_trend_RxXd_RXXp_region_season(index_vec[7],"region")

pdf(file=paste(path_plot,"trends_region.pdf",sep=""),width=plotwd+3,height=plotht+8)
ggarrange(p1, 
          ggarrange(p2,p3,p4,p5,p6,p7,
                    labels=LETTERS[2:7], ncol=2, nrow=3, align="h"),
          labels="A", nrow=2, heights = c(0.3, 0.7),align = "v")
dev.off()

# Plot interaction of mean and Rx1d
trend_mean_RxXd()

# Create control map with EOBS-data:
plot_50yr_EOBS()

# Mean precip changes as function of season
plot_trend_mean_season()

# Extreme precip changes as a function of season:
index_vec <- c("Rx1d","Rx3d","Rx5d","Rx7d","Rx31d","#R95p","#R99p")
ls_plot <- lapply(index_vec,function(x) plot_trend_mean_season(x))
pdf(file=paste(path_plot,"trends_season.pdf",sep=""),width=plotwd+3,height=plotht+8)
ggarrange(ls_plot[[1]], 
          ggarrange(ls_plot[[2]],ls_plot[[3]],ls_plot[[4]],ls_plot[[5]],ls_plot[[6]],ls_plot[[7]],
                    labels=LETTERS[2:7], ncol=2, nrow=3, align="h"),
          labels="A", nrow=2, heights = c(0.3, 0.7)) #,align = "v")
dev.off()









