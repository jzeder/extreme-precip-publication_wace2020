READ ME - Scripts Extreme Precipitation Publication WACE 2020
=============================================================

1) *Input data:*

* The data can be downloaded from: https://doi.org/10.3929/ethz-b-000349713
* Set up the directory structure using: ```xargs mkdir -p < dirstruct.txt```

2) *Plots output:*

All plots in 'Output/Plots/', where the subfolder
structure represents partly the structure of the scripts and of the
chapters of the thesis:

* 1_ConversionExperiment -> Simulation experiment checking conversion from linear
                            trend to scaling

* 2_Metadata		         -> Different plots not directly related to scaling
					 	                or trend estimates

* 3_Nonpar_Trends	     	 -> All plots related to temporal trends

* 4_EVD_Trends	  	     -> All plots related to univariate scaling estimates
						                and field significance

* 5_Spatial_Clustering 	 -> All plots related to clustering and weighted
						                averaging

3) *Data output:*

All datasets in 'Output/Data/':
* df_res_temp    -> Temporary dataframes of results, only the last 19
				            are kept (the older ones are deleted regularly)

* df_res_master  -> Dataframe with all univariate results (which is
         				 	  updated with values in df_res_temp when one types
				 	          'join_dataframes(results=T)')

* \_resample     -> All resampling results are kept (not always the
				 	          resampled time series, as it uses to much space)

4) *How-to:*

- Presets in '0_Basic_Settings.R':
	In this script, many things are preset which are relevant for all
	scripts (otherwise, the presets are done in the header of the specific
	scripts)
- Run scripts:
	Always run header first, so that necessary paths are set and datasets
	are loaded.
- Apply "save_res" option:
	If set to true, results are saved in df_res_temp. These are concatenated
	to df_res_master if command is typed: 'join_dataframes(results=T)'
- Metadata on stations:
	Metadata on stations is saved in 'df_meta_d'
- Indices of seasons in df_xxxx_d dataframes:
	Indices of such stations are stored in list 'ind$<season>', e.g.
	'ind$JJA' for JJA maxima.
